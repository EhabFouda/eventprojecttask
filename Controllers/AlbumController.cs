﻿using Event.Application.Features.Album.Commands.CreateAlbum;
using Event.Application.Features.Album.Commands.DeleteAlbum;
using Event.Application.Features.Album.Commands.UpdateAlbum;
using Event.Application.Features.Album.Queries.GetAlbumDetails;
using Event.Application.Features.Album.Queries.GetAlbumList;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Event.Web.Controllers
{
    public class AlbumController : Controller
    {
        private readonly IMediator _mediator;

        public AlbumController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<List<GetAlbumListViewModel>>> GetListAlbums(Guid Id)
        {
            ViewBag.EventId = Id;
            return View(await _mediator.Send(new GetAlbumListQuery() { EventId = Id }));
        }

        [HttpGet]
        public async Task<ActionResult<GetAlbumDetailsViewModel>> GetCategoryById(Guid Id)
        {
            return View(await _mediator.Send(new GetAlbumDetailsQuery() { AlbumId = Id }));
        }

        [HttpGet]
        public IActionResult CreateAlbum(Guid EventId)
        {
            ViewBag.EventId = EventId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAlbum([FromForm] CreateAlbumCommand createAlbumCommand)
        {
            Guid id = await _mediator.Send(createAlbumCommand);

            return RedirectToAction(nameof(GetListAlbums), new { id = createAlbumCommand.EventId });
        }

        [HttpGet]
        public async Task<ActionResult<GetAlbumDetailsViewModel>> UpdateAlbum(Guid Id)
        {
            return View(await _mediator.Send(new GetAlbumDetailsQuery() { AlbumId = Id }));
        }

        [HttpPost]
        public async Task<ActionResult> UpdateAlbum([FromForm] UpdateAlbumCommand updateAlbumCommand)
        {

            await _mediator.Send(updateAlbumCommand);

            return RedirectToAction(nameof(GetListAlbums), new {Id = updateAlbumCommand.EventId});
        }


        [HttpPost]
        public async Task<ActionResult> DeleteAlbum(Guid id)
        {
            await _mediator.Send(new DeleteAlbumCommand() { AlbumId = id });

            return Ok(new { key = 1 });
        }
    }
}
