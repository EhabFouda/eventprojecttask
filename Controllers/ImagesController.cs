﻿using Event.Application.Common.Helper;
using Event.Application.Features.Images.Commands.CreateImages;
using Event.Application.Features.Images.Commands.DeleteImages;
using Event.Application.Features.Images.Commands.UpdateImages;
using Event.Application.Features.Images.Queries.GetImagesDetails;
using Event.Application.Features.Images.Queries.GetImagesList;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Event.Web.Controllers
{
    public class ImagesController : Controller
    {
        private readonly IMediator _mediator;

        public ImagesController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpGet]
        public async Task<ActionResult<List<GetImagesListViewModel>>> GetListImages(Guid Id)
        {
            ViewBag.AlbumId = Id;
            return View(await _mediator.Send(new GetImagesListQuery() { AlbumId = Id }));
        }


        [HttpGet]
        public async Task<ActionResult<GetImagesDetailsViewModel>> GetImageById(Guid Id)
        {
            return View(await _mediator.Send(new GetImagesDetailsQuery() { ImageId = Id }));
        }


        [HttpGet]
        public IActionResult CreateImage(Guid AlbumId)
        {
            ViewBag.AlbumId = AlbumId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateImage([FromForm] CreateImagesCommand createAlbumCommand)
        {
            Guid id = await _mediator.Send(createAlbumCommand);

            return RedirectToAction(nameof(GetListImages), new { id = createAlbumCommand.AlbumId });
        }


        [HttpGet]
        public async Task<ActionResult<GetImagesDetailsViewModel>> UpdateImage(Guid Id)
        {
            return View(await _mediator.Send(new GetImagesDetailsQuery() { ImageId = Id }));
        }

        [HttpPost]
        public async Task<ActionResult> UpdateImage([FromForm] UpdateImagesCommand updateImageCommand)
        {
            await _mediator.Send(updateImageCommand);

            return RedirectToAction(nameof(GetListImages), new { Id = updateImageCommand.AlbumId });
        }

        [HttpPost]
        public async Task<ActionResult> DeleteImage(Guid id)
        {
            await _mediator.Send(new DeleteImagesCommand() { ImageId = id });

            return Ok(new { key = 1 });
        }
    }
}
