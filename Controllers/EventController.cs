﻿using Event.Application.Features.Event.Commands.CreateEvent;
using Event.Application.Features.Event.Commands.DeleteEvent;
using Event.Application.Features.Event.Commands.UpdateEvent;
using Event.Application.Features.Event.Queries.GetEventDetails;
using Event.Application.Features.Event.Queries.GetEventList;
using Event.Application.Features.Event.Queries.GetPostDetails;
using Event.Application.Features.Event.Queries.GetPostsList;
using Event.Domain.Entities;
using EventTask.Web.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Event.Web.Controllers
{

    public class EventController : Controller
    {
        private readonly IMediator _mediator;

        public EventController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<List<GetEventListViewModel>>> GetListEvents()
        {
            return View(await _mediator.Send(new GetEventListQuery()));
        }

        [HttpGet]
        public async Task<ActionResult<GetEventDetailsViewModel>> GetEventById(Guid Id)
        {
            return View(await _mediator.Send(new GetEventDetailsQuery() { EventId = Id}));
        }

        [HttpGet]
        public ActionResult CreateEvent()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateEvent([FromForm] CreateEventCommand createEventCommand)
        {
            Guid id = await _mediator.Send(createEventCommand);

            return RedirectToAction(nameof(GetListEvents));
        }

        [HttpPost]
        public async Task<ActionResult> DeleteEvent(Guid id)
        {
            await _mediator.Send(new DeleteEventCommand() { EventId = id });

            return Ok(new { key = 1 });
        }

        [HttpGet]
        public async Task<ActionResult<GetEventDetailsViewModel>> UpdateEvent(Guid Id)
        {
            return View(await _mediator.Send(new GetEventDetailsQuery() { EventId = Id }));
        }

        [HttpPost]
        public async Task<ActionResult> UpdateEvent([FromForm] UpdateEventCommand updateEventCommand)
        {
            await _mediator.Send(updateEventCommand);

            return RedirectToAction(nameof(GetListEvents));
        }
    }
}